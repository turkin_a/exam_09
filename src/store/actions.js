import * as actionTypes from './actionTypes';
import axios from '../axios-contacts';

export const loadContactList = contacts => {
  return {type: actionTypes.LOAD_CONTACT_LIST, contacts};
};

export const clearSelectedId = id => {
  return {type: actionTypes.CLEAR_SELECTED_ID, id};
};

export const editExistingContact = id => {
  return {type: actionTypes.EDIT_EXISTING_CONTACT, id};
};

export const getContactList = () => {
  return dispatch => {
    axios.get('contacts.json').then(response => {
      dispatch(loadContactList(response.data));
    }).catch(error => console.log(error));
  }
};

export const addNewContact = newContact => {
  return dispatch => {
    axios.post('contacts.json', newContact).then(() => dispatch(getContactList()));
  }
};

export const removeExistingContact = id => {
  return dispatch => {
    axios.delete(`contacts/${id}.json`).finally(() => {
      dispatch(getContactList());
      dispatch(clearSelectedId(id));
    });
  }
};

export const saveExistingContact = (contact, id) => {
  return dispatch => {
    axios.put(`contacts/${id}.json`, contact).then(() => {
      dispatch(getContactList());
      dispatch(clearSelectedId(id));
    });
  }
};