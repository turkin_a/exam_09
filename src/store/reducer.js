import * as actionTypes from './actionTypes';

const initialState = {
  contacts: {},
  editingContactId: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_CONTACT_LIST:
      return {...state, contacts: action.contacts};
    case actionTypes.EDIT_EXISTING_CONTACT:
      return {...state, editingContactId: action.id};
    case actionTypes.CLEAR_SELECTED_ID:
      return {...state, editingContactId: ''};
    default:
      return state;
  }
};

export default reducer;