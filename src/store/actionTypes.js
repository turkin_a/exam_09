export const LOAD_CONTACT_LIST = 'LOAD_CONTACT_LIST';
export const EDIT_EXISTING_CONTACT = 'EDIT_EXISTING_CONTACT';
export const CLEAR_SELECTED_ID = 'CLEAR_SELECTED_ID';