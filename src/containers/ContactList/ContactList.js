import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";

import './ContactList.css';
import Button from "../../components/UI/Button/Button";
import ContactItem from "../../components/ContactItem/ContactItem";
import Overlay from "../../components/UI/Overlay/Overlay";
import Modal from "../../components/UI/Modal/Modal";
import ViewContactForm from "../../components/ViewContactForm/ViewContactForm";

import {editExistingContact, getContactList, removeExistingContact} from "../../store/actions";

class ContactList extends Component {
  state = {
    selectedId: ''
  };

  componentDidMount() {
    this.props.getContacts();
  }

  viewContactInfo = id => {
    this.setState({selectedId: id});
  };

  editContactInfo = (event, action, id) => {
    this.setState({selectedId: ''});
    if (!action) return null;
    if (action === 'remove') this.props.removeContact(id);
    if (action === 'edit') {
      this.props.editContact(id);
      this.props.history.push('/edit-contact');
    }
  };

  render() {
    let modal = null;

    if (this.state.selectedId) modal = (
      <Overlay>
        <Modal>
          <ViewContactForm title="Contact info"
            id={this.state.selectedId}
            item={this.props.contacts[this.state.selectedId]}
            clicked={this.editContactInfo}
          />
        </Modal>
      </Overlay>
    );

    return (
      <Fragment>
        <div className="ContactList">
          <div className="Header">
            <h3 className="PageTitle">Contacts</h3>
            <NavLink to="/add-contact">
              <Button>Add new contact</Button>
            </NavLink>
          </div>
          {this.props.contacts ? Object.keys(this.props.contacts).map(id => (
            <ContactItem key={id} id={id}
              item={this.props.contacts[id]}
              clicked={this.viewContactInfo}
            />
          )) : <div>Contact list is empty</div>}
        </div>
        {modal}
      </Fragment>
    );
  };
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getContacts: () => dispatch(getContactList()),
    removeContact: (id) => dispatch(removeExistingContact(id)),
    editContact: (id) => dispatch(editExistingContact(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);