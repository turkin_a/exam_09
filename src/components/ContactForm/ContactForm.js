import React, {Component} from 'react';
import './ContactForm.css';
import Button from "../UI/Button/Button";

class ContactForm extends Component {
  state = {
    contact: {
      name: '',
      tel: '',
      email: '',
      url: ''
    },
    mayBeCreate: false
  };

  componentDidMount() {
    if (this.props.id) {
      this.setState({contact: this.props.contact, mayBeCreate: true});
    }
  }

  changeInputHandler = event => {
    const key = event.target.name;
    let contact = {...this.state.contact};
    contact[key] = event.target.value;
    let mayBeCreate = this.state.mayBeCreate;

    if (this.state.contact.name.length > 0 && key !== 'name') mayBeCreate = true;
    if (this.state.contact.name.length === 0 && key === 'name') mayBeCreate = true;
    if (key === 'name' && contact[key].length === 0) mayBeCreate = false;

    this.setState({contact, mayBeCreate});
  };

  render() {
    return (
      <form className="ContactForm">
        <div className="FormRow">
          <label><span className="LabelName">Name:</span>
            <input type="text" name="name"
                   value={this.state.contact.name}
                   onChange={(e) => this.changeInputHandler(e)}
            />
          </label>
        </div>
        <div className="FormRow">
          <label><span className="LabelName">Phone:</span>
            <input type="tel" name="tel"
                   value={this.state.contact.tel}
                   onChange={(e) => this.changeInputHandler(e)}
            />
          </label>
        </div>
        <div className="FormRow">
          <label><span className="LabelName">Email:</span>
            <input type="email" name="email"
                   value={this.state.contact.email}
                   onChange={(e) => this.changeInputHandler(e)}
            />
          </label>
        </div>
        <div className="FormRow">
          <label><span className="LabelName">Photo:</span>
            <input type="url" name="url"
                   value={this.state.contact.url}
                   onChange={(e) => this.changeInputHandler(e)}
            />
          </label>
        </div>
        <div className="FormRow-preview">
          <span>Preview photo:</span>
          <div className="Photo-wrap">
            <img src={this.state.contact.url} alt=""/>
          </div>
        </div>
        <div className="FormRow FormRow-buttons">
          <Button disabled={!this.state.mayBeCreate}
                  classes={['WithoutBorder', 'Submit']}
                  clicked={(e) => this.props.clicked(e, {...this.state.contact}, this.props.id)}
          >{this.props.okBtnNane}</Button>
          <Button classes={['WithoutBorder', 'Danger']}
                  clicked={(e) => this.props.clicked(e)}
          >CANCEL</Button>
        </div>
      </form>
    );
  }
}

export default ContactForm;