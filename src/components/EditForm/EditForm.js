import React, {Component} from 'react';
import {connect} from "react-redux";

import './EditForm.css';
import ContactForm from "../ContactForm/ContactForm";
import {saveExistingContact} from "../../store/actions";

class EditForm extends Component {
  buttonClicked = (event, contact, id) => {
    event.preventDefault();
    if (!id) {
      this.props.history.push('/');
      return null;
    }
    this.props.saveContact(contact, id);
    this.props.history.push('/');
  };

  render() {
    return (
      <div className="EditForm">
        <h3 className="PageTitle">Edit contact</h3>
        <ContactForm okBtnNane={'SAVE'}
          id={this.props.editingContactId}
          contact={this.props.contacts[this.props.editingContactId]}
          clicked={this.buttonClicked}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    editingContactId: state.editingContactId
  }
};

const mapDispatchToProps = dispatch => {
  return {
    saveContact: (contact, id) => dispatch(saveExistingContact(contact, id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);