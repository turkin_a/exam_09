import React from 'react';
import './ViewContactForm.css';
import Button from "../UI/Button/Button";

const ViewContactForm = props => {
  return (
    <div className="ViewContactForm">
      <div className="HeadRow">
        <span className="Close" onClick={(e) => props.clicked(e)}>CLOSE</span>
      </div>
      <div className="ContactInfo">
        <div className="Avatar-wrap">
          <img src={props.item.url} alt=""/>
        </div>
        <div className="ContactInfoRow">
          <span className="ContactInfoItem Name">{props.item.name}</span>
          <span className="ContactInfoItem">Phone: {props.item.tel}</span>
          <span className="ContactInfoItem">Email: {props.item.email}</span>
        </div>
      </div>
      <div className="Buttons">
        <Button classes={['WithoutBorder', 'Submit']}
                clicked={(e) => props.clicked(e, 'edit', props.id, props.item)}
        >EDIT</Button>
        <Button classes={['WithoutBorder', 'Danger']}
                clicked={(e) => props.clicked(e, 'remove', props.id)}
        >DELETE</Button>
      </div>
    </div>
  )
};

export default ViewContactForm;