import React, {Component} from 'react';
import {connect} from "react-redux";

import './AddNewForm.css';
import ContactForm from "../ContactForm/ContactForm";
import {addNewContact} from "../../store/actions";

class AddNewForm extends Component {
  buttonClicked = (event, newContact) => {
    event.preventDefault();
    if (newContact) this.props.addContact(newContact);
    this.props.history.push('/');
  };

  render() {
    return (
      <div className="AddNewForm">
        <h3 className="PageTitle">Add new contact</h3>
        <ContactForm okBtnNane={'CREATE'} clicked={this.buttonClicked} />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addContact: (newContact) => dispatch(addNewContact(newContact))
  };
};

export default connect(null, mapDispatchToProps)(AddNewForm);