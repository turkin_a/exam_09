import React from 'react';
import './ContactItem.css';

const ContactItem = props => {
  return (
    <div className="ContactItem" onClick={() => props.clicked(props.id)}>
      <div className="Avatar-wrap">
        <img src={props.item.url} alt=""/>
      </div>
      <span className="ContactName">{props.item.name}</span>
    </div>
  )
};

export default ContactItem;