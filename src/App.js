import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";

import './App.css';
import ContactList from "./containers/ContactList/ContactList";
import AddNewForm from "./components/AddNewForm/AddNewForm";
import EditForm from "./components/EditForm/EditForm";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact component={ContactList} />
        <Route path="/add-contact" component={AddNewForm} />
        <Route path="/edit-contact" component={EditForm} />
        <Route render={() => <h1>404 Not found</h1>} />
      </Switch>
    );
  }
}

export default App;
